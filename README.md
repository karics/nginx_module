# NGINX Module with shared memory communication #

It is a possible implementation of an NGINX module that can send HTTP arguments, headers, and body through shared memory. The shared memory contains a ring buffer. Shared mutex and conditional variables act as synchronization between processes.

The repository contains the module itself, a ring buffer implementation with unit tests, and a pure POSIX based shared memory, mutex, conditional variable implementations. Two sample applications (client and server) added. They are using the shared_memory_communication library for communication.


It supports only Linux.
Tested on Ubuntu 18.04 LTS

### Repository ###
Third parties:
* NGINX 1.14.0 in the root
* gtest and gmock in ngx_http_passthrough_shared_memory_module/thirdparty for unit tests
* ngx_http_hello_world: hello world module based on https://tejgop.github.io/nginx-module-guide/#introduction

### Setup ###

0. Install prerequisite packages (Install what is came up during the build)
1. Start build_module.sh (This script copy the compiled module to /usr/local/modules folder. Modify it if you have NGINX on another place)
2. Compile server application inside the shared_memory_application folder via CMake. Unit tests and test client added too.
3. Start server
4. Start NGINX

### TODO ###

* Implement serialization
* Add conditional variable instead of sleep when the ring buffer is full (ngx_http_passthrough_shared_memory_module.c:~162)
* Support configuration for shared memory size and shared memory names.
* Add multiple producer/consumer support. (Currently, it supports 1 producer and 1 consumer)
* Reorganize project (Move thirdparty folder to root folder. Move nginx source to thirdparty folder.)