#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>

//Shared memory communication headers

#include "shared_memory_communication/error_handling.h"
#include "shared_memory_communication/constants.h"
#include "shared_memory_communication/ringbuffer.h"
#include "shared_memory_communication/shared_memory.h"
#include "shared_memory_communication/shared_mutex.h"
#include "shared_memory_communication/shared_conditional_variable.h"

static char *ngx_http_passthrough_shared_memory(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);

static ngx_command_t ngx_http_passthrough_shared_memory_commands[] = {
    {
        ngx_string("forward_through_shared_memory"),
        NGX_HTTP_LOC_CONF|NGX_CONF_NOARGS,
        ngx_http_passthrough_shared_memory,
        0,
        0,
        NULL
    },
    ngx_null_command
};

static ngx_http_module_t ngx_http_passthrough_shared_memory_module_ctx = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};


static ngx_int_t init_module(ngx_cycle_t *cycle);

static void exit_module(ngx_cycle_t *cycle);
//static ngx_int_t init_process(ngx_cycle_t *cycle) { return NGX_OK; }
//static ngx_int_t init_thread(ngx_cycle_t *cycle) { return NGX_OK; }



ngx_module_t ngx_http_passthrough_shared_memory_module = {
    NGX_MODULE_V1,
    &ngx_http_passthrough_shared_memory_module_ctx,
    ngx_http_passthrough_shared_memory_commands,
    NGX_HTTP_MODULE,
    NULL,                         /* init master */
    NULL,                        /* init module */
    init_module,                         // init process ngx_int_t (*init_process)(ngx_cycle_t *cycle)
    NULL,                        /* init thread */
    NULL,                        /* exit thread */
    exit_module,                         // exit process void (*exit_process)(ngx_cycle_t *cycle)
    NULL,                        /* exit master */
    NGX_MODULE_V1_PADDING
};

#define LOG(logger, msg) ngx_log_error(NGX_LOG_NOTICE, logger, 0, msg);


struct ngx_http_shared_memory_communication
{
    struct ngx_http_module_shared_mutex smutex;
    struct ngx_http_module_shared_conditional_variable scond_var;
    struct ngx_http_module_shared_memory sm;
    struct ring_buffer* rb;

} smc;

static ngx_int_t init_module(ngx_cycle_t *cycle)
{
    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Init module");

    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Setup shared memory communication" );
    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Open shared mutex" );
    open_shared_mutex(&smc.smutex, NGX_HTTP_MODULE_MUTEX_NAME);
    
    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Open shared conditional variable" );
    
    open_shared_conditional_variable(&smc.scond_var, NGX_HTTP_MODULE_CV_NAME);


    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Open shared memory" );
    
        open_shared_memory(&smc.sm, O_WRONLY, 
        NGX_HTTP_MODULE_SHARED_MEMORY_NAME, 
        NGX_HTTP_MODULE_SHARED_MEMORY_SIZE);

    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Initialize ring buffer" );
    smc.rb = (struct ring_buffer*)smc.sm.ptr;

    return NGX_OK; 
}

static void exit_module(ngx_cycle_t *cycle) 
{
    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Exit module");

    ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "Teardown shared memory communication" );
    close_shared_memory(&smc.sm);
    close_shared_conditional_variable(&smc.scond_var);
    close_shared_mutex(&smc.smutex);
}

void start_sending_session(ngx_log_t* logger)
{
    int retval = pthread_mutex_lock(smc.smutex.mutex);
    if(retval != 0)
    {
        LOG( logger, "Cannot lock shared mutex!" ); //TODO: Handle error outside of this function
    }
}

void end_sending_session(ngx_log_t* logger)
{
    int retval = pthread_mutex_unlock(smc.smutex.mutex);
    if(retval != 0)
    {
        LOG(logger,  "Cannot unlock shared mutex!" ); //TODO: Handle error outside of this function
    }
}


//Enter criteria: ring buffer mutex locked
//Exit criteria: ring buffer mutex locked
void send_data(const u_char* buf, unsigned int size, ngx_log_t* logger)
{
    int written_byte_count = 0;
    int remaining_data = size;
    //Split up buffer if it is not fit into ringbuffer
    while(remaining_data != 0)
    {
        int remaining_space = smc.rb->size - ring_buffer_size(smc.rb);
        int writable_data_size = remaining_data <= remaining_space ? remaining_data : remaining_space;
        if(writable_data_size != 0)
        {
            if( ring_buffer_push(smc.rb, (char* )buf + written_byte_count, writable_data_size) != 0 )
            {
                LOG( logger, "Not enough space in ring buffer" );
            }

            int retval = pthread_cond_signal(smc.scond_var.cv);
            if(retval != 0)
            {
                LOG( logger, "Cannot send signal to conditional variable!" );
            }

            written_byte_count += writable_data_size;
            remaining_data -= writable_data_size;
        }

        if(writable_data_size == 0)
        {
            //TODO: add conditional variable which triggered by the server
            end_sending_session(logger);
            LOG(logger, "Cannot write more into shared memory. Wait a little ...");
            sleep(1); //Give time to read it
            LOG(logger, "Wait ended");
            start_sending_session(logger);
        }
    }    
}

static void ngx_http_passthrough_shared_memory_body_handler(ngx_http_request_t *r)
{
    ngx_log_t* logger = r->connection->log;

    if(r->request_body == NULL)
    {
        LOG(logger, "Body is NULL");
        return;
    }

    start_sending_session(logger);

    LOG(logger, "Send Body");
    send_data((u_char*)"Body: \n", 8, logger);

    if(r->request_body->temp_file == NULL)
    {
        ngx_chain_t *chained_buf = r->request_body->bufs;
        if(chained_buf == NULL)
        {
            LOG(logger, "Empty body! Nothing to send!")
        }
        else
        {
            LOG(logger, "Send body from memory buffer");
            while(chained_buf != NULL)
            {
                send_data(chained_buf->buf->pos, chained_buf->buf->last - chained_buf->buf->pos, logger);
                chained_buf = chained_buf->next;
            }
        }       
    }
    else
    {
        LOG(logger, "Send body from temp file");
        ngx_temp_file_t* temp_file = r->request_body->temp_file;
        
        if(temp_file->file.fd == NGX_INVALID_FILE)
        {
            LOG(logger, "Invalid file.fd");
        }
        else
        {
            u_char* buf;
            const int buffer_size = 4096;
            buf = ngx_alloc(buffer_size,logger);
            int read_count = 0;
            do{
                read_count = ngx_read_fd(temp_file->file.fd, buf, buffer_size);
                send_data(buf, read_count, logger);
            }
            while(read_count == buffer_size);
            ngx_free(buf);
        }
    }

    end_sending_session(logger);
}

static ngx_int_t ngx_http_passthrough_shared_memory_handler(ngx_http_request_t *r)
{
    ngx_log_t* logger = r->connection->log;
    LOG(logger, "Handler start");

    u_char *response_body_text = (u_char*) "Hello from nginx module!";
    size_t sz = strlen((char*)response_body_text);

    r->headers_out.content_type.len = strlen("text/html")-1;
    r->headers_out.content_type.data = (u_char *) "text/html";
    r->headers_out.status = NGX_HTTP_OK;
    r->headers_out.content_length_n = sz;
    ngx_http_send_header(r);

//Send arguments, headers and body through shared memory
//TODO: Move output formatting into server application (make serialize/deserialize function)
    start_sending_session(logger);

    LOG( r->connection->log, "Send Arguments");
    send_data((u_char*)"Arguments: \n", 13, logger);
    send_data(r->args.data, r->args.len,logger);
    send_data((u_char*)"\n", 2, logger);
    
    LOG( r->connection->log, "Send Headers");
    send_data((u_char*)"Headers: \n", 11, logger);
    
    ngx_list_part_t* part = &(r->headers_in.headers.part);
    ngx_table_elt_t* header;
    while(part != NULL)
    {
        header = part->elts;
        for(unsigned int i = 0; i < part->nelts; i++)
        {
            send_data(header[i].key.data, header[i].key.len, logger);
            send_data((u_char*)" : ", 3, logger);
            send_data(header[i].value.data, header[i].value.len, logger);
            send_data((u_char*)"\n", 1, logger);
        }
        part = part->next;
    }

    end_sending_session(logger);

    ngx_http_read_client_request_body(r,ngx_http_passthrough_shared_memory_body_handler);

    ngx_buf_t *b;
    ngx_chain_t *out;

    b = ngx_calloc_buf(r->pool);
    out = ngx_alloc_chain_link(r->pool);

    out->buf = b;
    out->next = NULL;

    b->pos = response_body_text;
    b->last = response_body_text + sz;
    b->memory = 1;
    b->last_buf = 1;

    ngx_int_t rv = ngx_http_output_filter(r,out);
    ngx_http_finalize_request(r, rv);
    LOG(logger, "Handler end");
    return rv;
}

static char *ngx_http_passthrough_shared_memory(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_http_core_loc_conf_t *clcf;
    clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
    clcf->handler = ngx_http_passthrough_shared_memory_handler;

    return NGX_CONF_OK;
}
