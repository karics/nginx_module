cmake_minimum_required(VERSION 3.10)

project(shared_memory)

set(LIBRARY_OUTPUT_PATH "lib")
add_library(shared_memory STATIC 
        error_handling.c
        ringbuffer.c
        shared_memory.c
        shared_mutex.c
        shared_conditional_variable.c
         )

#IPC test apps
set(EXECUTABLE_OUTPUT_PATH "bin")
add_executable(server ./apps/server/server_main.cpp)
target_link_libraries(server shared_memory rt pthread)

set(EXECUTABLE_OUTPUT_PATH "bin")
add_executable(client ./apps/client/client_main.cpp)
target_link_libraries(client shared_memory rt pthread)

#Unit Test
set(LIBRARY_OUTPUT_PATH "lib")
add_library(gtest STATIC 
        ./thirdparty/googletest-master/googletest/src/gtest-all.cc
         )
target_include_directories(gtest PUBLIC 
        ./thirdparty/googletest-master/googletest 
        ./thirdparty/googletest-master/googletest/include)

set(LIBRARY_OUTPUT_PATH "lib")
add_library(gmock STATIC 
        ./thirdparty/googletest-master/googlemock/src/gmock-all.cc
         )
target_include_directories(gmock PUBLIC 
        ./thirdparty/googletest-master/googletest 
        ./thirdparty/googletest-master/googlemock 
        ./thirdparty/googletest-master/googletest/include 
        ./thirdparty/googletest-master/googlemock/include)

set(EXECUTABLE_OUTPUT_PATH "bin")
add_executable(test_shared_memory
        ./test/test_ringbuffer.cpp 
        ./thirdparty/googletest-master/googlemock/src/gmock_main.cc)
target_link_libraries(test_shared_memory shared_memory gmock gtest pthread)
