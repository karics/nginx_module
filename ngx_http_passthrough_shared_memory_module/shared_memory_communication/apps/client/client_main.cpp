#include <iostream>

#include <string>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

extern "C"
{
    #include "../../error_handling.h"
    #include "../../constants.h"
    #include "../../ringbuffer.h"
    #include "../../shared_memory.h"
    #include "../../shared_mutex.h"
    #include "../../shared_conditional_variable.h"
}

#define TEST_DATA_SIZE 256

void initialize_test_data(char** ptr)
{
    *ptr = (char*)malloc(TEST_DATA_SIZE);

    for(int i = 0; i < TEST_DATA_SIZE; i++)
    {
        (*ptr)[i] = 'a' + i % 26;
    }
}

void destroy_test_data(char* ptr)
{   
    free(ptr);
}

int main()
{
    std::cout << "Client started\n";
    std::cout << "Open shared mutex\n";
    char* test_data;
    initialize_test_data(&test_data);
    
    ngx_http_module_shared_mutex smutex;
    open_shared_mutex(&smutex, NGX_HTTP_MODULE_MUTEX_NAME);

    std::cout << "Open shared conditional variable\n";
    ngx_http_module_shared_conditional_variable scond_var;
    open_shared_conditional_variable(&scond_var, NGX_HTTP_MODULE_CV_NAME);

    std::cout << "Open shared memory\n";

    ngx_http_module_shared_memory sm;
        open_shared_memory(&sm, O_WRONLY, 
        NGX_HTTP_MODULE_SHARED_MEMORY_NAME, 
        NGX_HTTP_MODULE_SHARED_MEMORY_SIZE);

    ring_buffer* rb = (ring_buffer*)sm.ptr;

    int retval = 0;

    for( int i = 0; i < 10; i++)
    {
        std::cout << "Round: " << i << std::endl;
        test_data[0] = '0' + i % 10;
        
        int written_byte_count = 0;
        int remaining_data = TEST_DATA_SIZE;
        //Split up buffer if it is not fit into ringbuffer
        while(remaining_data != 0)
        {
            //std::cout << "Lock mutex\n";
            retval = pthread_mutex_lock(smutex.mutex);
            if(retval != 0)
            {
                std::cerr << "Cannot lock shared mutex: " << retval << std::endl;
            }

            int remaining_space = rb->size - ring_buffer_size(rb);
            int writable_data_size = remaining_data <= remaining_space ? remaining_data : remaining_space;
            if(writable_data_size != 0)
            {
                std::cout << "Send: " << std::string(test_data + written_byte_count, 5) << " ... \n";
                std::cout << "Size: " << writable_data_size << std::endl;
                if( ring_buffer_push(rb, test_data + written_byte_count, writable_data_size) != 0 )
                {
                    std::cerr << "Not enough space in ring buffer\n";
                }

                std::cout << "Signal conditional variable\n";
                retval = pthread_cond_signal(scond_var.cv);
                if(retval != 0)
                {
                    std::cerr << "Cannot send signal to conditional variable: " << retval << std::endl;
                }

                written_byte_count += writable_data_size;
                remaining_data -= writable_data_size;
            }
            else
            {
                
            }
            
            retval = pthread_mutex_unlock(smutex.mutex);
            if(retval != 0)
            {
                std::cerr << "Cannot unlock shared mutex: " << retval << std::endl;
            }

            if(writable_data_size == 0)
            {
                sleep(1); //Give time to read it
            }
        }


        //sleep(1);
    }

    close_shared_memory(&sm);
    close_shared_conditional_variable(&scond_var);
    close_shared_mutex(&smutex);

    destroy_test_data(test_data);

    return 0;
}
