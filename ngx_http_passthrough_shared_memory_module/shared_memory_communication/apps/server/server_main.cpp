#include <iostream>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>

extern "C" {
    #include "../../constants.h"
    #include "../../error_handling.h"
    #include "../../ringbuffer.h"
    #include "../../shared_memory.h"
    #include "../../shared_mutex.h"
    #include "../../shared_conditional_variable.h"
}

bool isRunning = true;

ngx_http_module_shared_mutex smutex;
ngx_http_module_shared_conditional_variable scond_var;
ngx_http_module_shared_memory sm;


pthread_t consumer_thread;

void cleanup()
{
    std::cout << "Start cleanup\n";
    pthread_mutex_unlock(smutex.mutex);
    
    close_shared_memory(&sm);
    close_shared_conditional_variable(&scond_var);
    close_shared_mutex(&smutex);
}

void signal_handler(int s)
{
    std::cout << "Signal received: " << s << std::endl;
    isRunning = false;
    
    std::cout << "Cancel thread\n";
    pthread_cancel(consumer_thread);
    cleanup();
}

static void* consume(void * ignored_arguments)
{
    std::cout << "Create shared mutex\n";
    
    create_shared_mutex(&smutex, NGX_HTTP_MODULE_MUTEX_NAME);
    int retval = pthread_mutex_lock(smutex.mutex);
    
    if(retval != 0)
    {
        std::cerr << "Cannot lock shared mutex: " << retval << std::endl;
        exit(1);
    }

    std::cout << "Create shared conditional variable\n";
    create_shared_conditional_variable(&scond_var, NGX_HTTP_MODULE_CV_NAME );

    std::cout << "Create shared memory\n";
    
    create_shared_memory(&sm, NGX_HTTP_MODULE_SHARED_MEMORY_NAME, NGX_HTTP_MODULE_SHARED_MEMORY_SIZE);


    std::cout << "Initialize ring buffer in shared memory\n";
    ring_buffer* rb;
    rb = ring_buffer_init_on_allocated_address(sm.ptr, NGX_HTTP_MODULE_SHARED_MEMORY_SIZE);
    if(rb == NULL)
    {
        std::cerr << "Cannot initialize ring buffer in shared memory!\n";
        exit(1);        
    }

    retval = pthread_mutex_unlock(smutex.mutex);
    if(retval != 0)
    {
        std::cerr << "Cannot unlock shared mutex: " << retval << std::endl;
        exit(1);
    }

    char temp[4096];

    std::cout << "Start infinite loop\n";

    while(isRunning)
    {

        std::cout << "Lock mutex\n";
        retval = pthread_mutex_lock(smutex.mutex);
        if(retval != 0)
        {
            std::cerr << "Cannot lock shared mutex: " << retval << std::endl;
        }

        int retval =  pthread_cond_wait(scond_var.cv, smutex.mutex) != 0;
        if( retval != 0 )
        {
            std::cout << "Cond wait error! Error code: " << retval << std::endl;
            exit(1);
            break;
        }

        int receiveDataCount = ring_buffer_pop(rb, temp, 4096);
       
        if(receiveDataCount)
        {
            std::string receivedText(temp, receiveDataCount);

            std::cout << "Received data size: " << receiveDataCount << std::endl;
            std::cout << "Received data: " << receivedText << std::endl;
        }

        retval = pthread_mutex_unlock(smutex.mutex);
        if(retval != 0)
        {
            std::cerr << "Cannot unlock shared mutex: " << retval << std::endl;
        }
    }

    cleanup();
}


int main()
{
    std::cout << "Server started\n";

//Setup signal handlers
    struct sigaction sig_handler;
    sig_handler.sa_handler = signal_handler;
    
    sigemptyset(&sig_handler.sa_mask);
    sig_handler.sa_flags = 0;

    sigaction(SIGINT, &sig_handler, NULL);
    sigaction(SIGTERM, &sig_handler, NULL);
    sigaction(SIGQUIT, &sig_handler, NULL);

    std::cout << "Start thread\n";
    int retval = pthread_create(&consumer_thread, NULL, &consume, NULL);

    if(retval != 0)
    {
        std::cerr << "Error during thread creation! Error code: " << retval << std::endl;
        exit(1);
    }

    void * res;
    retval = pthread_join(consumer_thread, &res);
    if(retval != 0)
    {
        std::cerr << "Error during thread jon! Error code: " << retval << std::endl;
    }
    if(res==PTHREAD_CANCELED)
    {
        std::cout << "Thread cancelled!" << std::endl;
    }
    else
    {
        std::cout << "Thread ended!" << std::endl;
    }
    

    return 0;
}