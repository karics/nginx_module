#ifndef _NGX_HTTP_MODULE_CONSTANTS_H_
#define _NGX_HTTP_MODULE_CONSTANTS_H_

#define NGX_HTTP_MODULE_SHARED_MEMORY_SIZE 4096 //Size of shared memory
#define NGX_HTTP_MODULE_SHARED_MEMORY_NAME "ngx_http_module_shared_memory" //Unique name of shared memory

#define NGX_HTTP_MODULE_MUTEX_NAME "ngx_http_module_mutex"
#define NGX_HTTP_MODULE_CV_NAME "ngx_http_module_conditional_variable"


#endif