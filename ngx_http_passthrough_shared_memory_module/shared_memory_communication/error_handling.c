#include "error_handling.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>


void report_and_exit(const char* msg)
{
    perror(msg);
    exit(-1);
}

void report_error(int val, const char* msg)
{
    if(val != 0)
    {
        printf("%s Error code: %d",msg, val);
    }
}
