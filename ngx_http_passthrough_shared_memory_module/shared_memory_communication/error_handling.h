#ifndef _NGX_HTTP_MODULE_ERROR_HANDLING_H_
#define _NGX_HTTP_MODULE_ERROR_HANDLING_H_

void report_and_exit(const char* msg);
void report_error(int val, const char* msg);

#endif