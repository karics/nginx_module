#include "ringbuffer.h"

#include <stdlib.h>
#include <string.h>

struct ring_buffer* ring_buffer_init_on_allocated_address( void* start_address, unsigned int size)
{
    if( size < sizeof(struct ring_buffer))
        return NULL;
    
    struct ring_buffer* rb = (struct ring_buffer*)start_address;
    rb->head = 0;
    rb->tail = 0;
    rb->buffer_owner = 0;
    rb->buffer = NULL; //Buffer not allocated -> NULL
    rb->size = size - sizeof(struct ring_buffer);

    return rb;
}

int ring_buffer_init( struct ring_buffer* rb, unsigned int size )
{
    char* ptr = (char*)malloc(size);
    if(ptr == NULL)
    {
        return ECANNOTALLOCATE;
    }

    rb->size = size;
    rb->buffer = ptr; 
    rb->buffer_owner = 1;
    rb->head = 0;
    rb->tail = 0;
    return 0;
}

void ring_buffer_destroy( struct ring_buffer* rb )
{
    if(rb->buffer_owner == 1)
    {
        free(rb->buffer);
    }
    rb->buffer = NULL;
    rb->size = 0;
    rb->head = 0;
    rb->tail = 0;
    rb->buffer_owner = 0;
}

char* ring_buffer_get_buffer(struct ring_buffer* rb)
{
    if(rb->buffer_owner)
    {
        return rb->buffer;
    }
    else
    {
        return (char*)(rb + 1); //Buffer starts after the ring_buffer object
    }

}

//Returns 0 on success
int ring_buffer_push( struct ring_buffer* rb, const char* buf, unsigned int size )
{
    //Is there enough space?
    if( size > rb->size - ring_buffer_size(rb) )
        return ENOTENOUGHSPACE;

    if(rb->tail <= rb->head)
    {
        //Is there enough space from the head till the end?
        if( rb->head + size <= rb->size )
        {
            memcpy(ring_buffer_get_buffer(rb) + rb->head, buf, size);
            rb->head += size;
        }
        else
        {
            unsigned int first_part_size = rb->size - rb->head;
            unsigned int remaining_size = size - first_part_size;
            //copy the buffer till the end
            memcpy(ring_buffer_get_buffer(rb) + rb->head, buf, first_part_size);
            //Copy the remaining buffer from the begin
            memcpy(ring_buffer_get_buffer(rb), buf + first_part_size, remaining_size);
            rb->head = remaining_size - 1;
        }
    }
    else
    {
        memcpy(ring_buffer_get_buffer(rb) + rb->head, buf, size);
        rb->head += size;
    }
    
    return 0;
}

//Returns the number of bytes popped (maximum size) or error
int ring_buffer_pop( struct ring_buffer* rb, char* buf, unsigned int size )
{
    unsigned int current_size = ring_buffer_size(rb);
    unsigned int size_to_pop = current_size < size ? current_size : size;

    if( rb->tail + size_to_pop <= rb->size )
    {
        memcpy(buf, ring_buffer_get_buffer(rb) + rb->tail, size_to_pop );
        rb->tail += size_to_pop;
    }
    else
    {
        unsigned int first_part = rb->size - rb->tail;
        unsigned int remaining_part = size_to_pop - first_part;

        memcpy(buf, ring_buffer_get_buffer(rb) + rb->tail, first_part);
        memcpy(buf + first_part, ring_buffer_get_buffer(rb), remaining_part);

        rb->tail = remaining_part - 1;
    }
    return size_to_pop;
    
}

unsigned int ring_buffer_size( struct ring_buffer* rb )
{
    if(rb->tail == rb->head)
        return 0;

    if( rb->tail < rb->head)
    {
        return rb->head - rb->tail;
    }
    else
    {
        return rb->head + rb->size - rb->tail + 1;
    }   
}