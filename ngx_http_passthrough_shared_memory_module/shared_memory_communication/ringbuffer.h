#ifndef _NGX_HTTP_MODULE_RINGBUFFER_H_
#define _NGX_HTTP_MODULE_RINGBUFFER_H_

#define ENOTENOUGHSPACE -1
#define ECANNOTALLOCATE -2

struct ring_buffer
{
    unsigned int head;
    unsigned int tail;
    unsigned int size;
    int buffer_owner;
    char* buffer;
};

//Not allocating rung_buffer just uses start_address 
//Returns NULL if not enough size to use as ring_buffer. Size have to be larger than sizeof(struct ring_buffer)
//the beginning of start_address will used as ring_buffer the remaining part will be the buffer
struct ring_buffer* ring_buffer_init_on_allocated_address( void* start_address, unsigned int size);

int ring_buffer_init( struct ring_buffer* rb, unsigned int size );
void ring_buffer_destroy( struct ring_buffer* rb );

int ring_buffer_push( struct ring_buffer* rb, const char* buf, unsigned int size ); //Returns 0 on success
int ring_buffer_pop( struct ring_buffer* rb, char* buf, unsigned int size ); //Returns the number of bytes popped (maximum size) or error
unsigned int ring_buffer_size( struct ring_buffer* rb );

char* ring_buffer_get_buffer(struct ring_buffer* rb);

#endif