#include "shared_conditional_variable.h"
#include "error_handling.h"

#include "fcntl.h"


void create_shared_conditional_variable(struct ngx_http_module_shared_conditional_variable *scv, const char* name )
{
    scv->owner = 1;
    create_shared_memory(&(scv->shared_memory), name, sizeof(pthread_cond_t));
    scv->cv = (pthread_cond_t*)(scv->shared_memory.ptr);

    //Shared cv between processes
    pthread_condattr_t cattr;
    pthread_condattr_init(&cattr);
    report_error( 
        pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_SHARED), 
        "Cannot initilize shared conditional variable");

        
    report_error( 
        pthread_cond_init(scv->cv, &cattr),
        "Cannot initilize shared conditional variable");

    pthread_condattr_destroy(&cattr);
}

void open_shared_conditional_variable(struct ngx_http_module_shared_conditional_variable *scv, const char* name )
{
    scv->owner = 0;
    open_shared_memory(&(scv->shared_memory), O_RDWR, name, sizeof(pthread_cond_t));
    scv->cv = (pthread_cond_t*)(scv->shared_memory.ptr);
}

void close_shared_conditional_variable(struct ngx_http_module_shared_conditional_variable *scv )
{
    if(scv->owner == 1)
    {
        pthread_cond_destroy(scv->cv);
    }
    scv->cv = NULL;
    close_shared_memory(&(scv->shared_memory));
}
