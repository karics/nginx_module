#ifndef _NGX_HTTP_MODULE_SHARED_CONDITIONAL_VARIABLE_H_
#define _NGX_HTTP_MODULE_SHARED_CONDITIONAL_VARIABLE_H_

#include <pthread.h>

#include "shared_memory.h"

struct ngx_http_module_shared_conditional_variable
{
    pthread_cond_t *cv;
    struct ngx_http_module_shared_memory shared_memory;
    int owner;
};

void create_shared_conditional_variable(struct ngx_http_module_shared_conditional_variable *scv, const char* name );
void open_shared_conditional_variable(struct ngx_http_module_shared_conditional_variable *scv, const char* name );
void close_shared_conditional_variable(struct ngx_http_module_shared_conditional_variable *scv );

#endif