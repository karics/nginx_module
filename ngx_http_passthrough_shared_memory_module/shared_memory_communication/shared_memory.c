#include "shared_memory.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "error_handling.h"

void create_shared_memory(struct ngx_http_module_shared_memory* sm, const char * name, int length)
{
    //umask settings: it is affects the shared memory permission
    //Store old umask
    mode_t old_umask = umask(0);

    
    sm->fd = shm_open( name, O_CREAT | O_RDWR | O_TRUNC, 0777);

    //Restore old umask
    umask(old_umask);
    if(sm->fd < 0)
    {
        report_and_exit("Can't create shared mem segment");
    }
    if( ftruncate(sm->fd, length) != 0 )
    {
        report_and_exit("ftruncate error");
    }


    sm->ptr = mmap(0, length, PROT_WRITE | PROT_READ , MAP_SHARED, sm->fd, 0);
    if( sm->ptr == MAP_FAILED ) 
    {
        report_and_exit("Can't map shared memory");
    }

    sm->name = name;
    sm->size = length;
    sm->owner = 1;
}


void open_shared_memory(struct ngx_http_module_shared_memory* sm, int flags, const char* name, int length)
{
    sm->fd = shm_open( name, O_RDWR, 0777);
    if(sm->fd < 0)
    {
        report_and_exit("Can't open shared mem segment");
    }

    if( ftruncate(sm->fd, length) != 0 )
    {
        report_and_exit("ftruncate error");
    }

    sm->ptr = mmap(0, length, PROT_WRITE | PROT_READ, MAP_SHARED, sm->fd, 0);
    if( sm->ptr == MAP_FAILED )
    {
        report_and_exit("Can't map shared memory");
    }

    sm->name = name;
    sm->size = length;
    sm->owner = 0;
}

void close_shared_memory(struct ngx_http_module_shared_memory* sm)
{
    if(sm->owner == 1)
    {
        shm_unlink(sm->name);
    }
    munmap(sm->ptr, sm->size);
}
