#ifndef _NGX_HTTP_MODULE_SHARED_MEMORY_H_
#define _NGX_HTTP_MODULE_SHARED_MEMORY_H_


struct ngx_http_module_shared_memory
{
    const char* name;
    int size;
    int fd; //Shared memory file descriptor
    void *ptr; //Pointer to shared memory
    int owner; //1 if this owns the shared memory
};


void create_shared_memory(struct ngx_http_module_shared_memory* sm, const char* name, int length);
void open_shared_memory(struct ngx_http_module_shared_memory* sm, int flags, const char* name, int length);
void close_shared_memory(struct ngx_http_module_shared_memory* sm);


#endif