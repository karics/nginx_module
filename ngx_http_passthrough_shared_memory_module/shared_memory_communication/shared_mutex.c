#include "shared_mutex.h"
#include "error_handling.h"

#include <fcntl.h>


void create_shared_mutex(struct ngx_http_module_shared_mutex *smutex, const char* name )
{
    smutex->owner = 1;
    create_shared_memory(&(smutex->shared_memory), name, sizeof(pthread_mutex_t));
    smutex->mutex = (pthread_mutex_t *)(smutex->shared_memory.ptr);

    //Share mutex between processes
    pthread_mutexattr_t mattr;

    pthread_mutexattr_init(&mattr);

    report_error( 
        pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED),
        "Cannot set mutexattr");
    
    report_error( 
        pthread_mutex_init(smutex->mutex, &mattr), 
        "Cannot initialize shared mutex");   
    
    pthread_mutexattr_destroy(&mattr);
}

void open_shared_mutex(struct ngx_http_module_shared_mutex *smutex, const char* name )
{
    smutex->owner = 0;
    open_shared_memory(&(smutex->shared_memory), O_RDWR, name, sizeof(pthread_mutex_t));
    smutex->mutex = (pthread_mutex_t *)(smutex->shared_memory.ptr);
}

void close_shared_mutex(struct ngx_http_module_shared_mutex *smutex )
{
    if(smutex->owner == 1)
    {
        pthread_mutex_destroy(smutex->mutex);
    }
    smutex->mutex = NULL;
    close_shared_memory(&(smutex->shared_memory));
}
