#ifndef _NGX_HTTP_MODULE_SHARED_MUTEX_H_
#define _NGX_HTTP_MODULE_SHARED_MUTEX_H_

#include <pthread.h>

#include "shared_memory.h"

struct ngx_http_module_shared_mutex
{
    pthread_mutex_t *mutex;
    struct ngx_http_module_shared_memory shared_memory;
    int owner;
};

void create_shared_mutex(struct ngx_http_module_shared_mutex *smutex, const char* name );
void open_shared_mutex(struct ngx_http_module_shared_mutex *smutex, const char* name );
void close_shared_mutex(struct ngx_http_module_shared_mutex *smutex );


#endif