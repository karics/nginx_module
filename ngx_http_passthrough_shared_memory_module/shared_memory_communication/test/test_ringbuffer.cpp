#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>

extern "C"
{
    #include "../ringbuffer.h"
}

TEST(RingBufferInitDestroyTests, InitTest)
{
    unsigned int size = 12;
    ring_buffer rb;
    ring_buffer_init(&rb, size);

    EXPECT_EQ(0, rb.head);
    EXPECT_EQ(0, rb.tail);
    EXPECT_TRUE(rb.buffer_owner);
    EXPECT_EQ(size, rb.size);
    EXPECT_TRUE(NULL != rb.buffer);

    ring_buffer_destroy(&rb); //Avoid mem leak
}


TEST(RingBufferInitDestroyTests, DestroyTest)
{
    unsigned int size = 12;
    ring_buffer rb;
    ring_buffer_init(&rb, size);

    ring_buffer_destroy(&rb);

    EXPECT_EQ(0, rb.head);
    EXPECT_EQ(0, rb.tail);
    EXPECT_EQ(0, rb.size);
    EXPECT_FALSE(rb.buffer_owner);
    EXPECT_TRUE(NULL == rb.buffer);
}

TEST(RingBufferInitDestroyWithAllocatedStorageTests, InitTest)
{
    unsigned int size = 12;
    unsigned int storage_size = 12+sizeof(ring_buffer);
    char allocated_storage[storage_size];
    memset(&allocated_storage, 1, storage_size);

    ring_buffer* rb;
    rb = ring_buffer_init_on_allocated_address(&allocated_storage, storage_size);

    EXPECT_EQ(0, rb->head);
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(size, rb->size);
    EXPECT_FALSE(rb->buffer_owner);
    EXPECT_EQ( NULL, rb->buffer);
    EXPECT_EQ( (char*)((char*)rb + sizeof(ring_buffer)) ,ring_buffer_get_buffer(rb) );
    

    ring_buffer_destroy(rb); //Avoid mem leak

    EXPECT_EQ(0, rb->head);
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(0, rb->size);
    EXPECT_FALSE(rb->buffer_owner);
    EXPECT_TRUE( NULL == rb->buffer);
}

//true if allocate
class RingBufferTest : public ::testing::TestWithParam<bool>
{
protected:
    ring_buffer* rb = nullptr;
    unsigned int size = 50;
    char storage[50+sizeof(ring_buffer)];
    void SetUp() override 
    {
        if(GetParam())
        {
            rb = new ring_buffer();
            ring_buffer_init(rb, size);
        }
        else
        {
            rb = ring_buffer_init_on_allocated_address(&storage, 50+sizeof(ring_buffer));
            ASSERT_TRUE(rb != NULL);
        }
    }

    void TearDown() override
    {
        ring_buffer_destroy(rb);
        if(GetParam())
        {
            delete rb;
        }
    }

    //Full: tail = 30, head = 29
    //Not full: tail = 30, head = 9
    void makeTailBehindHead(bool shouldBeFull)
    {
        char custom_data[40] = {
            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
            20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39};
        ring_buffer_push(rb, custom_data, 40);
        char temp[30];
        ring_buffer_pop(rb, temp, 30);
        if(shouldBeFull)
            ring_buffer_push(rb, custom_data, 40);
        else
            ring_buffer_push(rb, custom_data, 20);
    }

};

INSTANTIATE_TEST_SUITE_P(UseAllocatedStorage, RingBufferTest, ::testing::Bool());

TEST_P(RingBufferTest, EmtpySizeTest)
{
    EXPECT_EQ(0, ring_buffer_size(rb));
}

TEST_P(RingBufferTest, SimplePushTest)
{
    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);

    EXPECT_EQ(0, retval); //No error

    EXPECT_EQ(buf_size, ring_buffer_size(rb));
    EXPECT_EQ(buf_size, rb->head);
    EXPECT_THAT( std::vector<char>( ring_buffer_get_buffer(rb), ring_buffer_get_buffer(rb) + rb->head )
    , ::testing::ElementsAre(0,1,2,3,4,5,6,7,8,9));
    //These are not changed
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, DoublePushTest)
{
    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);

    EXPECT_EQ(0, retval); //No error

    retval = ring_buffer_push(rb, custom_data, buf_size);

    EXPECT_EQ(0, retval); //No error

    EXPECT_EQ(buf_size * 2, ring_buffer_size(rb));
    EXPECT_EQ(buf_size * 2, rb->head);
    EXPECT_THAT( std::vector<char>( ring_buffer_get_buffer(rb), ring_buffer_get_buffer(rb) + rb->head )
    , ::testing::ElementsAre(0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9));
    //These are not changed
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PushTheExactSizeTimeTest)
{
    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    ring_buffer_push(rb, custom_data, buf_size);
    ring_buffer_push(rb, custom_data, buf_size);
    ring_buffer_push(rb, custom_data, buf_size);
    ring_buffer_push(rb, custom_data, buf_size);
    int retval = ring_buffer_push(rb, custom_data, buf_size);
    EXPECT_EQ(0, retval); //No error

    EXPECT_EQ(buf_size * 5, ring_buffer_size(rb));
    EXPECT_EQ(buf_size * 5, rb->head);
    //These are not changed
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PushTooMuchTimesTest)
{
    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    ring_buffer_push(rb, custom_data, buf_size);
    ring_buffer_push(rb, custom_data, buf_size);
    ring_buffer_push(rb, custom_data, buf_size);
    ring_buffer_push(rb, custom_data, buf_size);
    ring_buffer_push(rb, custom_data, buf_size);

    //Everything remain the same
    EXPECT_EQ(buf_size * 5, ring_buffer_size(rb));
    EXPECT_EQ(buf_size * 5, rb->head);
    //These are not changed
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(size, rb->size);

    int retval = ring_buffer_push(rb, custom_data, buf_size);
    EXPECT_EQ(ENOTENOUGHSPACE, retval); //No error

    //Everything remain the same
    EXPECT_EQ(buf_size * 5, ring_buffer_size(rb));
    EXPECT_EQ(buf_size * 5, rb->head);
    //These are not changed
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PopEmptyTest)
{
    unsigned int test_buf_size = 10;
    char test_buf[test_buf_size] = {0,0,0,0,0,0,0,0,0,0};
    int retval = ring_buffer_pop(rb, test_buf, test_buf_size);
    EXPECT_EQ(0,retval); //Nothing copied

//Initialized state kept
    EXPECT_EQ(0, ring_buffer_size(rb));
    EXPECT_EQ(0, rb->head);
    EXPECT_EQ(0, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PushPopTest)
{
    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);

    EXPECT_EQ(0, retval); //No error

    char custom_data2[buf_size] = {0,0,0,0,0,0,0,0,0,0};
    retval = ring_buffer_pop(rb, custom_data2, 2);
    
    EXPECT_EQ(2, retval);

    EXPECT_EQ(buf_size - 2, ring_buffer_size(rb));
    EXPECT_EQ(2, rb->tail);
    EXPECT_THAT( std::vector<char>( custom_data2, custom_data2 + buf_size )
    , ::testing::ElementsAre(0,1,0,0,0,0,0,0,0,0));
    //These are not changed
    EXPECT_EQ(buf_size, rb->head);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PopTwiceTest)
{
    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);

    EXPECT_EQ(0, retval); //No error

    char custom_data2[buf_size] = {0,0,0,0,0,0,0,0,0,0};
    retval = ring_buffer_pop(rb, custom_data2, 2);    
    EXPECT_EQ(2, retval);

    char custom_data3[buf_size] = {0,0,0,0,0,0,0,0,0,0};
    retval = ring_buffer_pop(rb, custom_data3, 2);    
    EXPECT_EQ(2, retval);

//4 element removed
    EXPECT_EQ(buf_size - 4, ring_buffer_size(rb));
    EXPECT_EQ(4, rb->tail);

    EXPECT_THAT( std::vector<char>( custom_data3, custom_data3 + buf_size )
    , ::testing::ElementsAre(2,3,0,0,0,0,0,0,0,0));
    //These are not changed
    EXPECT_EQ(buf_size, rb->head);
    EXPECT_EQ(size, rb->size);
}


TEST_P(RingBufferTest, PushPopTheSameTest)
{
    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);

    EXPECT_EQ(0, retval); //No error

    char custom_data2[buf_size] = {0,0,0,0,0,0,0,0,0,0};
    retval = ring_buffer_pop(rb, custom_data2, buf_size);
    EXPECT_EQ(buf_size, retval);
    
    EXPECT_EQ(0, ring_buffer_size(rb));
    EXPECT_EQ(rb->head, rb->tail);
    EXPECT_THAT( std::vector<char>( custom_data2, custom_data2 + buf_size )
    , ::testing::ElementsAre(0,1,2,3,4,5,6,7,8,9));
    //These are not changed
    EXPECT_EQ(buf_size, rb->head);
    EXPECT_EQ(size, rb->size);
}


TEST_P(RingBufferTest, TailBehindHeadFullTest)
{
    //Full: tail = 30, head = 29
    makeTailBehindHead(true);
    EXPECT_EQ(29, rb->head);
    EXPECT_EQ(30, rb->tail);
    EXPECT_EQ(50, ring_buffer_size(rb));
}

TEST_P(RingBufferTest, TailBehindHeadNotFullTest)
{
    //Not full: tail = 30, head = 9
    makeTailBehindHead(false);
    EXPECT_EQ(9, rb->head);
    EXPECT_EQ(30, rb->tail);
    EXPECT_EQ(30, ring_buffer_size(rb));
}
TEST_P(RingBufferTest, PushWhenTailBehindHeadTest)
{
    makeTailBehindHead(false);

    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);

    EXPECT_EQ(0, retval); //No error

    EXPECT_EQ(40, ring_buffer_size(rb));
    EXPECT_EQ(19, rb->head);
    EXPECT_THAT( std::vector<char>( ring_buffer_get_buffer(rb) + 9, ring_buffer_get_buffer(rb) + 19 )
    , ::testing::ElementsAre(0,1,2,3,4,5,6,7,8,9));
    //These are not changed
    EXPECT_EQ(30, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PushToFullWhenTailBehindHeadTest)
{
    makeTailBehindHead(false);

    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);
    retval = ring_buffer_push(rb, custom_data, buf_size);
    EXPECT_EQ(0, retval); //No error

    EXPECT_EQ(50, ring_buffer_size(rb));
    EXPECT_EQ(29, rb->head);
    //These are not changed
    EXPECT_EQ(30, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PushThroughFullWhenTailBehindHeadTest)
{
    makeTailBehindHead(false);

    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,1,2,3,4,5,6,7,8,9};
    int retval = ring_buffer_push(rb, custom_data, buf_size);
    retval = ring_buffer_push(rb, custom_data, buf_size);
    EXPECT_EQ(0, retval); //No error
    retval = ring_buffer_push(rb, custom_data, buf_size);
    EXPECT_EQ(ENOTENOUGHSPACE, retval); //No error

    //These are not changed
    EXPECT_EQ(50, ring_buffer_size(rb));
    EXPECT_EQ(29, rb->head);
    EXPECT_EQ(30, rb->tail);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PopSmallWhenTailBehindHeadTest)
{
    makeTailBehindHead(false);

    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,0,0,0,0,0,0,0,0,0};
    int retval = ring_buffer_pop(rb, custom_data, buf_size);
    EXPECT_EQ(10, retval); //No error

    EXPECT_EQ(20, ring_buffer_size(rb));
    EXPECT_EQ(40, rb->tail);
    EXPECT_THAT( std::vector<char>( custom_data, custom_data + buf_size)
        , ::testing::ElementsAre(30,31,32,33,34,35,36,37,38,39));
    //These are not changed
    EXPECT_EQ(9, rb->head);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PopTillTheEndWhenTailBehindHeadTest)
{
    makeTailBehindHead(false);

    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,0,0,0,0,0,0,0,0,0};
    int retval = ring_buffer_pop(rb, custom_data, buf_size);
    EXPECT_EQ(10, retval);
    retval = ring_buffer_pop(rb, custom_data, buf_size);
    EXPECT_EQ(10, retval);

    EXPECT_EQ(10, ring_buffer_size(rb));
    EXPECT_EQ(50, rb->tail);
        EXPECT_THAT( std::vector<char>( custom_data, custom_data + buf_size )
    , ::testing::ElementsAre(0,1,2,3,4,5,6,7,8,9));
    //These are not changed
    EXPECT_EQ(9, rb->head);
    EXPECT_EQ(size, rb->size);
}

TEST_P(RingBufferTest, PopTillEmptyWhenTailBehindHeadTest)
{
    makeTailBehindHead(false);

    unsigned int buf_size = 10;
    char custom_data[buf_size] = {0,0,0,0,0,0,0,0,0,0};
    int retval = ring_buffer_pop(rb, custom_data, buf_size);
    EXPECT_EQ(10, retval);
    retval = ring_buffer_pop(rb, custom_data, buf_size);
    EXPECT_EQ(10, retval);
    retval = ring_buffer_pop(rb, custom_data, buf_size);
    EXPECT_EQ(10, retval);


    EXPECT_EQ(0, ring_buffer_size(rb));
    EXPECT_EQ(9, rb->tail);
    //These are not changed
    EXPECT_EQ(9, rb->head);
    EXPECT_EQ(size, rb->size);
}
